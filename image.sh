#!/bin/bash


if [ -z "$1"  ]; then
	imglist=`ls | grep -i ".jpg"`

	for img in $imglist; do
		check="${img:0:11}"

		if [[ "$check" != "__thumbnail" ]]; then
			W=`identify -format "%W" $img`
			H=`identify -format "%H" $img`

			if [ "$W" -gt "$H" ]; then
				filename="${img##*/}"
				filename="__thumbnail_$filename"
				cp "$img" "$filename"
				convert $filename -resize 360x"$H"\! $filename
			else
				filename="${img##*/}"
				filename="__thumbnail_$filename"
				cp "$img" "$filename"
				convert $filename -resize "$W"x360\! $filename
			fi
		fi
	done
	echo выполнено

else
	FILE=$1

	if [ -f "$FILE" ]; then
		imglist=`cat $1 | grep -i ".jpg"`
		
		for img in $imglist; do
			if [ -f "$img" ]; then
				W=`identify -format "%W" $img`
				H=`identify -format "%H" $img`

				if [ "$W" -gt "$H" ]; then
					filename="${img##*/}"
					filename="__thumbnail_$filename"
					cp "$img" "$filename"
					convert $filename -resize 360x"$H"\! $filename
				else
					filename="${img##*/}"
					filename="__thumbnail_$filename"
					cp "$img" "$filename"
					convert $filename -resize "$W"x360\! $filename
				fi
			fi
		done

		echo выполнено

	else

	echo "Файл $FILE не существует"
	fi

fi


